(Only members can translate, you want help ? ask us : pokemonmmo3d@gmail.com)

To translate something, you have to do:<br>
- Select your branch here https://framagit.org/pmmo-3d/translation/branches/active , for example if you want edit the English language, use the branch "english",<br>
- Go to the folder "English",<br>
- Select a file,<br>
- Edit the part between tags &lt;something&gt; & &lt;/something&gt;.